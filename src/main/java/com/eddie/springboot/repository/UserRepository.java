package com.eddie.springboot.repository;

import com.eddie.springboot.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);
    Optional<User> findByEmailContains(String email);
    Optional<User> findAllByEmail(String email);
    Optional<User> findAllByEmailContainsAndEmail(String email, String auth);
    Boolean existsByEmail(String email);
}
