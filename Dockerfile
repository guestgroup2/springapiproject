FROM openjdk:8-jdk-alpine
COPY xxx.jar /Documents/restapi/xxx.jar
WORKDIR /Documents/restapi
ADD . /Documents/restapi
RUN sh -c 'touch cbx-restapi-PAH.11.23.0P08.0.jar'
EXPOSE 3002
CMD java -jar xxx.jar
